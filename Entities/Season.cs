﻿using System.Runtime.Serialization;

namespace NineCodingChallenge.Entities
{
    [DataContract]
    public class Season
    {
        [DataMember]
        public string Slug;
    }
}
