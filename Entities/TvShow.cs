﻿using System.Runtime.Serialization;

namespace NineCodingChallenge.Entities
{
    [DataContract]
    public class TvShow
    {
        [DataMember]
        public string Country;
        [DataMember]
        public string Description;
        [DataMember]
        public bool Drm;
        [DataMember]
        public int? EpisodeCount;
        [DataMember]
        public string Genre;
        [DataMember]
        public Image Image;
        [DataMember]
        public string Language;
        [DataMember]
        public EpisodePreview NextEpisode;
        [DataMember]
        public string PrimaryColour;
        [DataMember]
        public Season[] Seasons;
        [DataMember]
        public string Slug;
        [DataMember]
        public string Title;
        [DataMember]
        public string TvChannel;
    }
}
