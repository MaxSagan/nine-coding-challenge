﻿using System;
using System.Runtime.Serialization;

namespace NineCodingChallenge.Entities
{
    [DataContract]
    public class EpisodePreview
    {
        [DataMember]
        public string Channel;
        [DataMember]
        public string ChannelLogo;
        [DataMember]
        public DateTime? Date;
        [DataMember]
        public string Html;
        [DataMember]
        public string Url;
    }
}
