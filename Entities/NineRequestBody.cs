﻿using System;
using System.Runtime.Serialization;

namespace NineCodingChallenge.Entities
{
    [DataContract]
    public class NineRequestBody
    {
        [DataMember]
        public TvShow[] Payload;
        [DataMember]
        public int Skip;
        [DataMember]
        public int Take;
        [DataMember]
        public int TotalRecords;
    }
}
