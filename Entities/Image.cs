﻿using System.Runtime.Serialization;

namespace NineCodingChallenge.Entities
{
    [DataContract]
    public class Image
    {
        [DataMember]
        public string ShowImage;
    }
}
