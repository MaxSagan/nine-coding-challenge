﻿using System;
using System.Runtime.Serialization;

namespace NineCodingChallenge.Entities
{
    [DataContract]
    public class NineResponseBody
    {
        [DataMember]
        public TvShowSummary[] Response;
    }
}
