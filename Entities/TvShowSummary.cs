﻿using System.Runtime.Serialization;

namespace NineCodingChallenge.Entities
{
    [DataContract]
    public class TvShowSummary
    {
        [DataMember]
        public string Image;
        [DataMember]
        public string Slug;
        [DataMember]
        public string Title;
    }
}
