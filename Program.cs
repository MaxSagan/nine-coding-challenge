﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace NineCodingChallenge
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("OMG");
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
