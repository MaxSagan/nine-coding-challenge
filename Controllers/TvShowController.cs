﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using NineCodingChallenge.Entities;

namespace NineCodingChallenge.Controllers
{
    [Route("api/[controller]/[action]")]
    public class TvShowController : Controller
    {
        [HttpPost]
        [ProducesResponseType(typeof(TvShowSummary[]), 200)]
        [ProducesResponseType(400)]
        public IActionResult Filter([FromBody]NineRequestBody requestBody)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new { error = "Could not decode request: JSON parsing failed?!" });
            }
            if (requestBody?.Payload == null)
            {
                return BadRequest(new { error = "Empty payload?!" });
            }
            var filtered = requestBody.Payload.Where(tvProgram =>
                tvProgram.Drm && tvProgram.EpisodeCount > 0
            );
            var summarized = filtered.Select(tvProgram =>
                new TvShowSummary
                {
                    Image = tvProgram.Image.ShowImage,
                    Title = tvProgram.Title,
                    Slug = tvProgram.Slug
                }
            );
            var responseBody = new NineResponseBody
            {
                Response = summarized.ToArray()
            };
            return new ObjectResult(responseBody);
        }
    }
}
